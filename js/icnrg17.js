
//~~~~~~~~~~~~OVERVIEW SLIDE~~~~~~~~~~~~~~~~~~~

function payments_1(){
  test.style.visibility="visible";
}

function payments_2(){
  $("#test")
    .velocity({translateX: 35, translateY: -15}, 2000)
}

function payments_3(){
  task.style.visibility="visible";
  money.style.visibility="visible";
}

function payments_4(){
  $("#task").delay(0)
    .velocity({translateX: 40, translateY: 20}, 2000)
  $("#money").delay(1500)
      .velocity({translateX: 30, translateY: 30}, 2000)
}

function payments_5(){
  developer.style.visibility="visible";
}

function payments_6(){
  test_ok.style.visibility="visible";
}

function payments_7(){
  $("#test_ok")
    .velocity({translateX: -35, translateY: -15}, 2000)
  animateGear.beginElement();
}

function payments_8(){
  question.style.visibility="visible";
  $("#question").delay(0)
    .velocity({translateX: 0, translateY: -50}, 1500)
    .velocity({opacity: 0}, { duration: 1000 }, {visibility: "hidden"})
}

function payments_9(){
  ok.style.visibility="visible";
  $("#ok").delay(0)
    .velocity({translateX: 0, translateY: 50}, 1500)
    .velocity({opacity: 0}, { duration: 1000 }, {visibility: "hidden"})
}

function payments_10(){
  $("#money")
    .velocity({translateX: 85, translateY: -10}, 2000)
}

function payments_11(){
  payments_money.style.visibility="visible";
  $("#payments_money").delay(1500)
    .velocity({translateX: 75, translateY: 32}, 1500);
}


function payments_12(){
  $("#payments_key").delay(500)
    .velocity({translateX: 89, translateY: -10}, 1500);
}

function payments_13(){
  $("#payments_money").delay(0)
    .velocity({translateX: 6, translateY: 70}, 1500);
  $("#payments_key").delay(1500)
    .velocity({translateX: 16, translateY: -40}, 1500);
  }

  function payments_14(){
    payments_result_user.style.visibility="visible";
    payments_secret.style.visibility="hidden";
  }

  function payments_15(){
    payments_channel.style.visibility="visible";
    payments_result_user.style.visibility="hidden";
    payments_secret_ok.style.visibility="hidden";
    payments_hash_ok.style.visibility="hidden";
    payments_data.style.visibility="visible";

    $("#payments_key").delay(0)
        .velocity({translateX: 0, translateY: 0}, 0);
    $("#payments_data").delay(0)
        .velocity({translateX: 0, translateY: 0}, 0);
    $("#payments_hash").delay(0)
        .velocity({translateX: 0, translateY: 0}, 0);
    $("#payments_money").delay(0)
        .velocity({translateX: 0, translateY: 0}, 0);
  }

  var inputID;
  var secretID;
  var moneyID;
  var keyID;

  function payments_16(){
    inputID=setInterval(payments_move_input, 5000);
    payments_secret.style.visibility="visible";
    $("#payments_secret").velocity({ opacity: 0 }, {duration: 0}, {visibility: "hidden"});
    secretID=setInterval(payments_move_secret, 5000);
    payments_money.style.visibility="visible";
    $("#payments_money").velocity({ opacity: 0 }, {duration: 0}, {visibility: "hidden"});
    moneyID=setInterval(payments_move_money, 6000);
    payments_key.style.visibility="visible";
    $("#payments_key").velocity({ opacity: 0 }, {duration: 0}, {visibility: "hidden"});
    keyID=setInterval(payments_move_key, 6000);
  }


  function payments_move_input(){
    $("#payments_data").delay(0)
      .velocity({translateX: 0, translateY: 0}, 500)
		  .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
      .velocity({translateX:-2, translateY: 61}, 2000)
      .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  }

  function payments_move_secret(){
    $("#payments_secret").delay(1000)
      .velocity({translateX: 0, translateY: 0}, 500)
		  .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
      .velocity({translateX:2, translateY: -61}, 2000)
      .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  }

  function payments_move_money(){
    $("#payments_money").delay(0)
      .velocity({translateX: 0, translateY: 0}, 500)
		  .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
      .velocity({translateX:25, translateY: 32}, 1500)
      .delay(500)
      .velocity({translateX:6, translateY: 70}, 1500)
      .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  }

  function payments_move_key(){
    $("#payments_key").delay(1000)
      .velocity({translateX: 0, translateY: 0}, 500)
      .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
      .velocity({translateX:16, translateY: -40}, 1500)
      .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  }

  function payments_17(){
    clearInterval(moneyID);
    clearInterval(secretID);
    clearInterval(keyID);
    clearInterval(inputID);

    $("#payments_secret").delay(0)
      .velocity({translateX: 0, translateY: 0}, 0)
		  .velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
      .velocity({translateX:2, translateY: -26}, 2000)
  }


  function payments_18(){
    payments_ipfs.style.visibility="visible";
    $("#payments_secret").delay(1500)
      .velocity({translateX: 77, translateY: 0}, 1500)
  }




/*function payments_1(){
  payments_hash.style.visibility="visible";
  payments_arrow.style.visibility="visible";
}*/

/*function payments_2(){
  payments_arrow.style.visibility="hidden";
  $("#payments_money").delay(1500)
    .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_deposit").delay(1000)
      .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_hash")
        .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_function")
        .velocity({translateX: 80, translateY: 10}, 1500);
}*/

/*function payments_3(){
  payments_deposit2.style.visibility="visible";
  $("#payments_deposit2").delay(1000)
    .velocity({translateX: 68, translateY: -28}, 1500);
}*/










/*function payments_9(){
  payments_result.style.visibility="visible";
  $("#payments_result").delay(500)
    .velocity({translateX:15, translateY: -10}, 2000);
  $("#payments_key").delay(2500)
      .velocity({translateX: 22, translateY: 60}, 2000);
}

function payments_10(){
  payments_animateGear1_1.endElement();
  payments_tls.style.visibility="hidden";
  payments_data.style.visibility="hidden";
  payments_enclave.style.visibility="hidden";
  $("#payments_key")
    .velocity({translateX: 85, translateY: 45}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  $("#payments_deposit2").delay(2500)
      .velocity({translateX: 0, translateY: 0}, 2000)
}

function payments_11(){
  $("#payments_result")
      .velocity({translateX: 15, translateY:-70}, 2000)
}

function payments_12(){
  //payments_yes.style.visibility="visible";
  $("#payments_yes")
    .velocity({translateX: 30, translateY: -45}, 0)
    .velocity({ opacity: 1 }, {duration: 0}, {visibility: "visible"})
    .delay(500)
    .velocity({translateX: 85, translateY: -25}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

function payments_13(){
  $("#payments_deposit")
      .velocity({translateX: -10, translateY: 0}, 1500);
  $("#payments_money").delay(2000)
          .velocity({translateX: 17, translateY: 70}, 1500);
}*/

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ INTEL SGX SLIDE~~~~~~~~~~~~~~~~
function sgx_1(){
  $("#key4_2")
    .velocity({translateX: 10, translateY: -50}, 2000);
}

function sgx_2(){
  $("#gear4_2")
    .velocity({translateX: 0, translateY: -15}, 1000)
    .velocity({translateX: 75, translateY: -15}, 1500)
    .velocity({translateX: 75, translateY: 0}, 1000);

    setTimeout(switchGears, 3500);
}

function switchGears(){
  gear4_2.style.visibility="hidden";
  gear4_3.style.visibility="visible";
}

function sgx_3(){
  animateGear4_3.beginElement();
  enclave4_1.style.visibility="visible";
}

function sgx_4(){
  $("#key4_1")
    .velocity({translateX: 0, translateY: -5}, 500)
    .velocity({translateX: 75, translateY: -5}, 2000);
}

function sgx_5(){
  secret4_1.style.visibility="visible";
  setInterval(movePackets4, 4000);
}

function movePackets4(){
  $("#secret4_1")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 500 }, {visibility: "visible"})
    .velocity({translateX: 10, translateY: 0}, 1000)
    .velocity({translateX: 10, translateY: 45}, 1800)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
